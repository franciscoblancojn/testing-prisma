const db = require('./db')

async function main() {
    const allUser = await db.getUser()
    console.log(allUser)
    db.disconnect()
}

main()