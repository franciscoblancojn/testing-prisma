//requires
const express = require('express'),
      bodyParser = require('body-parser');

const f = require('./functions')
const db = require('./db')

/**
 * @description port and rute
 */
const port = 3000;


/**
 * @description load app 
 */
var app = express();
app.set('port', port);
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(bodyParser.raw());
app.use(express.urlencoded({extended: false}));
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});
const prefixFunctions = {
    "get":"get",
    "post":"create",
    "put":"update",
    "delete":"delete"
}
const rute = '/';
[
    "user",
    "post",
    "profile"
].forEach(e => {
    [
        "get",
        "post",
        "put",
        "delete"
    ].forEach(j => {
        app[j](rute+e, async function(req, res) {
            try {
                const result = await db[`${prefixFunctions[j]}${f.capitalize(e)}`](req.body || {})
                db.disconnect()
                f.respond({
                    res,
                    responde:{
                        type:"ok",
                        result
                    }
                })
            } catch (error) {
                f.respond({
                    res,
                    responde:{
                        type:"error",
                        error:`${error}`
                    }
                })
            }
        });
    });
});


/**
 * app.listen
 * @description enpoint listen 
 */
app.listen(port, function() {
    console.log('ok');
});
