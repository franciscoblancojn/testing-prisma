const respond = ({res,responde}) => {
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify(responde));
    res.end();
} 
const capitalize = (s) => {
  if (typeof s !== 'string') return ''
  return s.charAt(0).toUpperCase() + s.slice(1)
}


const f = {
    respond,
    capitalize,
}

module.exports = f