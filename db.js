const {PrismaClient} = require('@prisma/client')


const prisma = new PrismaClient()
//User
    const createUser = async ({name,email}) => {
        return await prisma.user.create({
            data: {
            name,
            email,
            },
        })
    }
    const getUser = async ({query={}}) => {
        return await prisma.user.findMany(query)
    }
    const updateUser = async ({where,data}) => {
        return await prisma.user.update({
            where,
            data,
        })
    }
    const deleteUser = async ({where}) => {
        return await prisma.user.delete({
            where,
        })
    }
//Post
    const createPost = async ({title,content,published=false,authorId}) => {
        return await prisma.post.create({
            data: {
                title,
                content,
                published,
                authorId
            },
        })
    }
    const getPost = async ({query={}}) => {
        return await prisma.post.findMany(query)
    }
    const updatePost = async ({where,data}) => {
        return await prisma.post.update({
            where,
            data,
        })
    }
    const deletePost = async ({where}) => {
        return await prisma.post.delete({
            where,
        })
    }
//Profile
    const createProfile = async ({bio,userId}) => {
        return await prisma.profile.create({
            data: {
                bio,
                userId,
            },
        })
    }
    const getProfile = async ({query={}}) => {
        return await prisma.profile.findMany(query)
    }
    const updateProfile = async ({where,data}) => {
        return await prisma.profile.update({
            where,
            data,
        })
    }
    const deleteProfile = async ({where}) => {
        return await prisma.profile.delete({
            where,
        })
    }
//Disconnect
    const disconnect = async () => {
        await prisma.$disconnect()
    }

//db
const db = {
    createUser,
    getUser,
    updateUser,
    deleteUser,

    createPost,
    getPost,
    updatePost,
    deletePost,
    
    createProfile,
    getProfile,
    updateProfile,
    deleteProfile,

    disconnect,
}

module.exports = db